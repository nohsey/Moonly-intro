import 'package:intro_app/models/items.dart';

const itemList = [
  Item(title: "Soda", price: "€2,-", image: "assets/images/soda.png"),
  Item(title: "Gingerbread", price: "€0,50", image: "assets/images/gingerbread.png"),
  Item(title: "Bananas", price: "€1,-", image: "assets/images/bananas.png"),
  Item(title: "Avocado", price: "€2,25", image: "assets/images/avocado.png"),
  Item(title: "Chocolate", price: "€3,50", image: "assets/images/chocolate.png"),
  Item(title: "Loaf", price: "€1,50", image: "assets/images/loaf.png"),
  Item(title: "Pear", price: "€1,50", image: "assets/images/pear.png"),
  Item(title: "Apple", price: "€1,50", image: "assets/images/apple.png"),
  Item(title: "Tomato", price: "€0,75", image: "assets/images/tomato.png"),
  Item(title: "Chicken Leg", price: "€3,50", image: "assets/images/chickenleg.png"),
  Item(title: "Bell Pepper", price: "€1,-", image: "assets/images/bellpepper.png"),
  Item(title: "Coffee", price: "€3,-", image: "assets/images/coffee.png"),
  Item(title: "Lollipop", price: "€1,-", image: "assets/images/lollipop.png"),
];