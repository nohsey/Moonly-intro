import 'package:flutter/material.dart';
import 'package:intro_app/constants/colors.dart';
import 'package:intro_app/constants/string.dart';
import 'package:intro_app/features/common/button.dart';
import 'package:intro_app/features/product/widgets/relatedItemCard.dart';

class ProductScreen extends StatelessWidget {
  final String title;
  final String price;
  final String image;

  const ProductScreen({
    Key? key,
    required this.title, 
    required this.price, 
    required this.image,
    }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size; 
    return Scaffold(
      body: Stack(
        children: <Widget>[
          Container(
            height: size.height * .45,
            color: cSecondaryColor,
          ),
          SafeArea(
            child: Padding(
              padding: const EdgeInsets.all(30),
              child: Column(
                children: <Widget>[
                  
                  Align(
                      alignment: Alignment.topLeft,
                      child: IconButton(
                        icon: const Icon(Icons.arrow_back_ios),
                        onPressed: () {
                          Navigator.pop(context);
                        }, 
                      ),
                    ),
                    Image.asset(
                      image,
                      scale: .5,
                    ),
                    Container(
                      margin: EdgeInsets.symmetric(vertical: 30),
                      child: Column(
                        children: [
                        Row(
                          children: <Widget>[
                            Text(
                              title, 
                              style: Theme.of(context).textTheme.headline1,
                              textAlign: TextAlign.left
                            ),
                            Spacer(),
                            Text(
                              price, 
                              style: Theme.of(context).textTheme.headline2,
                              textAlign: TextAlign.left
                            ),
                          ]
                        ),
                        Text(
                          sProductDescription,
                          style: Theme.of(context).textTheme.bodyText1,
                          textAlign: TextAlign.left,
                        )
                      ],
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.symmetric(vertical: 20),
                    height: 180.0,
                    child: Column(
                      children: <Widget>[
                        Align(
                          alignment: Alignment.topLeft,
                          child: Text(
                            sRelatedTitle,
                            style: Theme.of(context).textTheme.subtitle1,
                            textAlign: TextAlign.left,
                          ), 
                        ),
                        Expanded(
                          child: ListView(
                          scrollDirection: Axis.horizontal,
                          children: <Widget>[
                            RelatedItemCard(
                              title: "Bananas", 
                              image: "assets/images/bananas.png"
                            ),
                            RelatedItemCard(
                              title: "Loaf", 
                              image: "assets/images/loaf.png"
                            ),
                            RelatedItemCard(
                              title: "Pear", 
                              image: "assets/images/pear.png"
                            ),
                            RelatedItemCard(
                              title: "Apple", 
                              image: "assets/images/apple.png"
                            ),
                            RelatedItemCard(
                              title: "Tomato", 
                              image: "assets/images/tomato.png"
                            ),
                          ],
                        )
                        ),
                      ],
                    )
                    // horizontal list
                  ),
                  Expanded(
                    child: Container()  // push to bottom of screen
                  ),
                  Container(
                    child: CustomButton(text: "Add to cart"),
                  )
                ]
              ),
            ),
          ),
        ]
      )
    );
  }
}
