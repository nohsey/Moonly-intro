import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:intro_app/constants/colors.dart';
import 'package:intro_app/features/product/product.dart';

class RelatedItemCard extends StatelessWidget {
  final String title;
  final String image;

  const RelatedItemCard({
    Key? key, 
    required this.title, 
    required this.image,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 5),
      width: 120,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(25),
          boxShadow: [
            BoxShadow(
              offset: Offset(0, 17),
              blurRadius: 17,
              spreadRadius: -23,
              color: cShadowColor
            )
          ]
        ),
        child: Material(
          color: Colors.transparent,
          child: GestureDetector(
            onTap: ()  {},
             child: InkWell(
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 10),
                child: Column(
                  children: <Widget>[
                    Spacer(),
                    Image.asset(image),
                    Spacer(),
                    Align(
                      alignment: Alignment.topLeft,
                      child: Text(
                        title, 
                        style: Theme.of(context).textTheme.subtitle2,
                        textAlign: TextAlign.left
                      ),
                    ),
                    Spacer(),
                  ],
                ),
              ),
            ),
          ),
         
        )
      );
  }
}