import 'package:flutter/material.dart';
import 'package:intro_app/constants/colors.dart';

class CustomButton extends StatelessWidget {
  final String text;

  const CustomButton({
    Key? key,
    required this.text
  }) : super (key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
      elevation: 0.8,
      borderRadius: BorderRadius.circular(25),
      child: InkWell(
        onTap: () {},
        child: Ink(
          decoration: BoxDecoration(
            color: cPrimaryColor,
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
          ),
          height: 48,
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Expanded(
                child: Center(
                  child: Text(
                    text,
                    style: Theme.of(context).textTheme.button,
                  ),
                ),
              ),
            ]
          ),
        ),
      ),
    );
  }
}