import 'dart:developer';

import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:intro_app/constants/colors.dart';
import 'package:intro_app/constants/string.dart';
import 'package:url_launcher/url_launcher.dart';

class AboutScreen extends StatefulWidget {
  const AboutScreen({Key? key}) : super(key: key);

  @override
  State<AboutScreen> createState() => _AboutScreenState();
}

class _AboutScreenState extends State<AboutScreen> {
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size; // height & width of device
    return Scaffold(
      body: Stack(
        children: <Widget>[
          Container(
            height: size.height * .45,
            color: cSecondaryColor,
          ),
          SafeArea(
            child: Padding(
              padding: const EdgeInsets.only(left: 30, right: 30, top: 30),
              child: Column(
                children: <Widget>[
                  Align(
                    alignment: Alignment.topLeft,
                    child: Text(
                      sAboutTitle, 
                      style: Theme.of(context).textTheme.headline1,
                      textAlign: TextAlign.left
                    ),
                  ),
                  Align(
                    alignment: Alignment.topLeft,
                    child: RichText(
                      text: TextSpan(
                        children: [
                          TextSpan(
                            text: sIllustrationCredit1,
                            style: Theme.of(context).textTheme.bodyText1, 
                          ),
                          TextSpan(
                            text: sIllustrationCredit2,
                            style: Theme.of(context).textTheme.bodyText2, 
                            recognizer: TapGestureRecognizer()
                            ..onTap = () { 
                              // launch url launcher
                              launch("https://icons8.com/illustrations");
                             },
                          ),
                          TextSpan(
                            text: sIllustrationCredit3,
                            style: Theme.of(context).textTheme.bodyText1, 
                          ),
                          TextSpan(
                            text: sIllustrationCredit4,
                            style: Theme.of(context).textTheme.bodyText2, 
                            recognizer: TapGestureRecognizer()
                            ..onTap = () { 
                              // launch url launcher
                              launch("https://icons8.com/illustrations/author/5c07e68d82bcbc0092519bb6a");
                             },
                          ),
                        ]
                      ),
                    ),
                  )
                ],
              )
            ),
            )
        ],
      )
    );
  }
}
