import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:intro_app/constants/colors.dart';
import 'package:intro_app/constants/data.dart';
import 'package:intro_app/constants/string.dart';
import 'package:intro_app/features/home/widgets/itemCard.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size; // height & width of device
    return Scaffold(
      body: Stack(
        children: <Widget>[
          Container(
            height: size.height * .45,
            color: cSecondaryColor,
          ),
          SafeArea(
            child: Padding(
              padding: const EdgeInsets.only(left: 30, right: 30, top: 30),
              child: Column(
                children: <Widget>[
                    Row(
                      children: <Widget>[
                        Text(
                          sCatalogTitle, 
                          style: Theme.of(context).textTheme.headline1,
                          textAlign: TextAlign.left
                        ),
                        Spacer(),
                        Container(
                          alignment: Alignment.center,
                          height: 45,
                          width: 45,
                          // ignore: prefer_const_constructors
                          decoration: BoxDecoration(
                            color: cPrimaryColor,
                            shape: BoxShape.circle
                            ),
                            child: SvgPicture.asset("assets/icons/menu.svg")
                        ),
                      ],
                    ),
                    Container(
                      margin: EdgeInsets.symmetric(vertical: 20),
                      padding: EdgeInsets.symmetric(horizontal: 10),
                      decoration: BoxDecoration(
                        color: Colors.white, 
                        borderRadius: BorderRadius.circular(10)
                        ),
                      child: TextField(
                        decoration: InputDecoration(
                          hintText: "Search",
                          icon: SvgPicture.asset("assets/icons/search.svg"),
                          border: InputBorder.none
                        ),
                      ),
                    ),
                    Expanded(
                      child: GridView.builder(
                        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                            crossAxisCount: 2,
                            crossAxisSpacing: 10,
                            mainAxisSpacing: 10,
                            childAspectRatio: .8,
                          ),
                        itemCount: itemList.length,
                        itemBuilder: (context, index) {
                          return  ItemCard(title: itemList[index].title, price: itemList[index].price, image: itemList[index].image);
                        }
                      )
                    )
                ],
              )
            ),
            )
        ],
      )
    );
  }
}
